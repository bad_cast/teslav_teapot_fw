#include "UART.h"
#include "UART_PROTOCOL.h"
#include "string.h"

#define FREQ		(unsigned long)			16000000
#define BAUDRATE	(unsigned long)			9600
#define N_BAUD		(FREQ/(BAUDRATE*64)-1)

static union uart_frame tx, rx, tx_buf, rx_buf;
static struct uart_status uart_status = {0};

static uint8_t wifi_led_status = 0;
static uint8_t beep_status = 0;
static uint8_t counter_led = 0, counter_buttons = 0;
static const uint8_t wifi_led_table[] = {0b00000000, 0b00000101, 0b11111111, 0b00110011, 0b00001111};
static const uint16_t beep_table[] = {0b0000000000000000, 0b0000000000000011, 0b0000000000110011, 0b0000001100110011, 0b0000000000000011};

//	calc frame checksum 
static uint8_t UART_crc(const union uart_frame *frame) {
	uint8_t sum = 0;
	uint8_t byte = 0;
	for (byte = 0; byte < sizeof(union uart_frame); byte++) {
		if (byte != (&frame->crc - &frame->raw[0])) {
			sum += frame->raw[byte];
		}
	}
	return sum;
}

void UART_init(void)
{
	_pac3 = 0;
	_pac1 = 1;

	_rx0ps1 = 0;
	_rx0ps0 = 1;
	_rx0en = 1;
	 
	_tx0ps1 = 0;
	_tx0ps0 = 1;
	
	_u0cr1 = 0b10000000;	//	uart enable, 1 stop bit
	_u0cr2 = 0b11000101;	//	tx/rx enable, tx/rx interrupts enable
	
	_brg0 = N_BAUD;
	
	_ur0f = 0;
	_ur0e = 1;

	//	vars init
	uart_status.send_status = 1;
	uart_status.frame_sent = 1;
	counter_4ms = 0;
	device_flags.WIFI_YES = 0;
	device_flags.WIFI_NO = 0;
	beep_status = BEEP_MODES - 1;
	uart_status.beep_next = 1;
}

//	UART0 INTERRUPT
DEFINE_ISR (INTERRUPT_UART, 0x28)
{	
	static uint8_t rx_frame_offset = 0;
	static uint8_t tx_frame_offset = 1;
	uint8_t b;
	
	//	tx
	if(_txif0) {
		if(!uart_status.frame_sent) {
			_txr_rxr0 = tx.raw[tx_frame_offset++];
			if (tx_frame_offset >= sizeof(tx)) {
				tx_frame_offset = 1;
				uart_status.frame_sent = 1;
			}
		}
	}
	//	rx
	if(_rxif0) {
		// Write byte to buffer
		rx_buf.raw[rx_frame_offset++] = _txr_rxr0;
		// Check that we read full frame
		if (rx_frame_offset != sizeof(rx_buf)) return;
		// Check frame header, end, checksum
		if ((rx_buf.type != UART_FRAME_TYPE_COMMAND && rx_buf.type != UART_FRAME_TYPE_CONNECTION) ||
			rx_buf.end != UART_FRAME_END ||
			rx_buf.crc != UART_crc(&rx_buf)) {
			// Frame is not valid, so we skip to next byte
			for (b = 0; b < sizeof(rx_buf) - 1; b++) {
				rx_buf.raw[b] = rx_buf.raw[b + 1];
			}
			rx_frame_offset--;
			return;
		}
	    // Frame is valid, so we can handle it
		for(b = 0; b < sizeof(rx); b++) {
			rx.raw[b] = rx_buf.raw[b];
		}
	    uart_status.frame_received = 1;
	    // Reset frame offset to skip unnecessary memory moves
	    rx_frame_offset = 0;
	}
}

void UART_form_status() {
	tx_buf.type = UART_FRAME_TYPE_STATUS;
	if (TEMP_MEASURED != 0) { // check that kettle is on base station (mmeasured temp is not 0)
		if (device_flags.WARM) {
			tx_buf.status.status = UART_DEVICE_STATUS_PROGRESS;
			if(device_flags.BOIL) {
				tx_buf.status.mode = UART_DEVICE_MODE_BOIL_THEN_KEEP;
			} else {
				tx_buf.status.mode = UART_DEVICE_MODE_WARM_THEN_KEEP;
			}
		} else {
			if(device_flags.BOIL) {
				tx_buf.status.status = UART_DEVICE_STATUS_PROGRESS;
				tx_buf.status.mode = UART_DEVICE_MODE_BOIL_THEN_IDLE;
			} else {
				tx_buf.status.status = UART_DEVICE_STATUS_IDLE;
				tx_buf.status.mode = UART_DEVICE_MODE_NONE;
			}
		}
		tx_buf.status.temperature = TEMP_TARGET*5 + 40;
		tx_buf.status.sensor_temperature._int = TEMP_MEASURED;
		tx_buf.status.sensor_temperature._frac = 0;
	} else {
		tx_buf.status.status = UART_DEVICE_STATUS_NOT_ON_BASE;
		tx_buf.status.mode = UART_DEVICE_MODE_NONE;
		tx_buf.status.temperature = 0;
		tx_buf.status.sensor_temperature._int = 0;
		tx_buf.status.sensor_temperature._frac = 0;
	}
}

void UART_form_action(enum uart_wifi_pairing_action action) {
	uint8_t b;
	tx_buf.type = UART_FRAME_TYPE_PAIRING;
	for(b = 0; b < sizeof(tx_buf.data); b++) {
		tx_buf.data[b] = 0;
	}
	tx_buf.pairing.action = action;
}

static void UART_handle_command(struct uart_device_command_frame *frame) {
	tx_buf.status.mode = frame->mode;
	switch (frame->mode) {
		case UART_DEVICE_MODE_STOP:
			// stop program that executing now, return to IDLE
			device_flags.WARM = 0;
			device_flags.BOIL = 0;
			break;
		case UART_DEVICE_MODE_BOIL_THEN_IDLE:
			// set device program to boil (100 C), then switch off
			device_flags.WARM = 0;
			device_flags.BOIL = 1;
			break;
		case UART_DEVICE_MODE_BOIL_THEN_KEEP:
			// set device program to boil (100 C), then keep temperature at frame->temperature
			device_flags.WARM = 1;
			device_flags.BOIL = 1;
			if(frame->temperature > 100)
				frame->temperature = 100;
			if(frame->temperature < 40)
				frame->temperature = 40;
			TEMP_TARGET = (frame->temperature - 40) / 5;
			break;
		case UART_DEVICE_MODE_WARM_THEN_IDLE:
			// set device program to warm upto frame->temperature, then switch off
			device_flags.WARM = 1;
			device_flags.BOIL = 0;
			if(frame->temperature > 100)
				frame->temperature = 100;
			if(frame->temperature < 40)
				frame->temperature = 40;
			TEMP_TARGET = (frame->temperature - 40) / 5;
			break;
		case UART_DEVICE_MODE_WARM_THEN_KEEP:
			// set device program to warm upto frame->temperature, then keep it
			device_flags.WARM = 1;
			device_flags.BOIL = 0;
			if(frame->temperature > 100)
				frame->temperature = 100;
			if(frame->temperature < 40)
				frame->temperature = 40;
			TEMP_TARGET = (frame->temperature - 40) / 5;
			break;
		case UART_DEVICE_MODE_PURIFY:
			// TODO(device): set device program to purify water (boil water, then keep boiling for 5 minutes)
			// TODO:	check if implemented
			break;
	}
	// Send current (updated) device status to UART
	uart_status.send_status = 1;
}

static void UART_handle_connection(const struct uart_wifi_connection_frame *frame) {
	switch (frame->state) {
		case UART_WIFI_CONNECTION_STATE_IDLE:
			// set connection indication mode to "IDLE"
			wifi_led_status = 0;
			break;
		case UART_WIFI_CONNECTION_STATE_FAILED:
			// set connection indication mode to "FAILURE"
			wifi_led_status = 1;
			break;
		case UART_WIFI_CONNECTION_STATE_CONNECTED:
			// set connection indication mode to "CONNECTED"
			wifi_led_status = 2;
			break;
		case UART_WIFI_CONNECTION_STATE_PAIRING:
			// set connection indication mode to "PAIRING"
			wifi_led_status = 3;
			break;
		case UART_WIFI_CONNECTION_STATE_DIAGNOSTICS:
			// set connection indication mode to "DIAGNOSTICS"
			wifi_led_status = 4;
			break;
	}
}

//	copy buf to frame and start tx
static void UART_tx_start() {
	uint8_t b;
	for(b = 0; b < sizeof(tx); b++) {
		tx.raw[b] = tx_buf.raw[b];
	}
	tx.end = UART_FRAME_END;
	tx.crc = UART_crc(&tx);
	//	initiate transmission
	_txr_rxr0 = tx.raw[0];
}

//	WIFI LED handler
static void UART_LED_handler() {
	static uint8_t counter = 0;
	if(uart_status.ind_next) {
		uart_status.ind_next = 0;
		device_flags.WIFI_YES = (wifi_led_table[wifi_led_status] >> counter++) & 1;
		if(counter >= WIFI_LED_STEPS) {
			counter = 0;
		}
	}
}

static void UART_beep_handler() {
	static uint8_t counter = 0;
	if(uart_status.beep_next) {
		uart_status.beep_next = 0;
		device_flags2.SPEAKER = (beep_table[beep_status] >> counter) & 1;
		if(beep_status) {
			counter++;
		}
		if(counter >= BEEP_STEPS) {
			counter = 0;
			beep_status = 0;
		}
	}
}

//	manage counters and +/- buttons pressed (pairing, diag, reset)
static void UART_counter_handler() {
	if(counter_4ms > 35) {	//	140 ms
		counter_4ms = 0;
		if(device_flags.WIFI_LK) {
			if (counter_buttons == BTN_CNT_RESET) {
				beep_status = 3;
				uart_status.action = UART_WIFI_PAIRING_ACTION_RESET;
				counter_buttons = BTN_CNT_RESET + 1;
			} else if (counter_buttons < BTN_CNT_RESET) {
				counter_buttons++;
				if(counter_buttons == BTN_CNT_PAIRING) {
					beep_status = 1;
				} else if (counter_buttons == BTN_CNT_DIAG) {
					beep_status = 2;
				}
			}
		}
		else {
			if(counter_buttons >= BTN_CNT_PAIRING && counter_buttons < BTN_CNT_DIAG) {
				uart_status.action = UART_WIFI_PAIRING_ACTION_PAIRING;
			}
			else if (counter_buttons >= BTN_CNT_DIAG && counter_buttons < BTN_CNT_RESET) {
				uart_status.action = UART_WIFI_PAIRING_ACTION_DIAGNOSTICS;
			}
			counter_buttons = 0;
		
		}
		uart_status.ind_next = 1;
		uart_status.beep_next = 1;
		if(++counter_led > 7) {	//	~1 sec
			counter_led = 0;
			uart_status.one_second_flag = 1;
		}
	}
}

void UART_handler() {
	//	manage counters and +/- buttons pressed (pairing, diag, reset)
	UART_counter_handler();
	//	manage LED
	UART_LED_handler();
	//	manage beep
	UART_beep_handler();
	//	target temperature changed
	if(uart_status.temp_target != TEMP_TARGET) {
		uart_status.temp_target = TEMP_TARGET;
		uart_status.send_status = 1;
	}
	//	measured temperature changed
	if(uart_status.temp_measured != TEMP_MEASURED) {
		uart_status.temp_measured = TEMP_MEASURED;
		uart_status.send_status = 1;
	}
	//	boil water pressed
	if(uart_status.boil!= device_flags.BOIL) {
		uart_status.boil = device_flags.BOIL;
		uart_status.send_status = 1;
	}
	//	warm water pressed
	if(uart_status.warm != device_flags.WARM) {
		uart_status.warm = device_flags.WARM;
		uart_status.send_status = 1;
	}
	//	1 second 
	if(uart_status.one_second_flag) {
		uart_status.one_second_flag = 0;
		uart_status.send_status = 1;
	}
	if(uart_status.frame_received) {
		switch (rx.type) {
			case UART_FRAME_TYPE_COMMAND:
				UART_handle_command(&rx.command);
				break;
			case UART_FRAME_TYPE_CONNECTION:
				UART_handle_connection(&rx.connection);
				break;
			default:
				break;
		}
		uart_status.frame_received = 0;
	}
	//	form and send tx frame
	if(_txif0 && uart_status.frame_sent) {
		if(uart_status.send_status) {
			uart_status.frame_sent = 0;
			UART_form_status();
			UART_tx_start();
			uart_status.send_status = 0;
		} else if (uart_status.action) {
			uart_status.frame_sent = 0;
			UART_form_action(uart_status.action);
			UART_tx_start();
			uart_status.action = 0;
		}
	}
}
