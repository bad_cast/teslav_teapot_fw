

message '***************************************************************'
message '*PROJECT NAME :USER PROGRAM CODE                              *'
message '*     VERSION :                                               *'
message '*     IC BODY :                                               *'
message '* ICE VERSION :                                               *'
message '*      REMARK :                                               *'
message '***************************************************************'

                INCLUDE USER_ASM.INC

                PUBLIC  _USER_PROGRAM_INITIAL_ASM
                PUBLIC  _USER_PROGRAM_ASM
                PUBLIC  _SJWD_RAM	; measured temperature (0..100)
                PUBLIC  _DW_RAM1	; target temperature (0..12 - 40..100 with step 5)
				PUBLIC  _BIT1		; flags
				PUBLIC  _BIT2		; flags
				PUBLIC  _COUNTER4MS	; counter 4 ms
                
                
                ;========================
                ;=USER DATA DEFINE      =
                ;========================
RAMBANK 0                
                
USER_DATA       .SECTION  'DATA' 


	TIME1D	 DB   ? ;	EQU		[0D0H]
	TIME1	 DB   ? ;	EQU		[0D0H]
	TIME2	 DB   ? ;	EQU		[0D1H]
	TIME3	 DB   ? ;	EQU		[0D2H]
	
	AD_RAMH	 DB   ? ;	EQU		[0D4H]
	AD_RAML	 DB   ? ;	EQU		[0D5H]
	RAM1	 DB   ? ;	EQU		[0D6H]	
	RAM2	 DB   ? ;	EQU		[0D7H]

	ZD_RAM0	 DB   ? ;	EQU		[0E0H]
	ZD_RAM1	 DB   ? ;	EQU		[0E1H]
	
	K_RAM1 		DB   ?
	K_RAM2 		DB   ?
	K_RAM3 		DB   ?
	K_RAM4 		DB   ?	

	SP_RAM		DB  ?
	AD_RAM_1	DB  ?	
	T_DATA		DB  ?
	DW_WD_RAM	DB  ?
	TIME3_A		DB  ?
	RT_TIEM 	DB  ?
	RT_DATA		DB  ?

	TIME2_D		DB  ?
	TIMEX	 	DB  ?

	BW_TIME_1	DB  ?
	BW_TIME_2	DB  ?
	;----------------------------------------
	DISP_RAM 	DB  ?	
	DISP_RAM3	DB  ?
	DISP_RAM2	DB  ?
	DISP_RAM1	DB  ?		
	DISP_RAM4_A	DB  ?	
	DISP_RAM3_A	DB  ?
	DISP_RAM2_A	DB  ?
	DISP_RAM1_A	DB  ?
	
	_COUNTER4MS	DB  ?


	BIT3		DB ?
	DW_TYPEQ	EQU		BIT3.0
	DW_TA		EQU		BIT3.1
	KEY_1A		EQU		BIT3.2		
	K_ON1		EQU		BIT3.3
	K_BIT1		EQU		BIT3.4		
	KEY_1		EQU		BIT3.5	
	AD1		 	EQU		BIT3.6		
	AD_BIT1		EQU		BIT3.7
	
		
	BIT4		DB ?
	TYPE_BIT1	EQU	BIT4.0			
	TYPE_BIT2 	EQU	BIT4.1
	TYPE_BIT3 	EQU	BIT4.2
	TYPE_BIT4	EQU	BIT4.3		
	DW_TYPE	  	EQU	BIT4.4
	YS_REAYT 	EQU	BIT4.6
	WT_100C		EQU	BIT4.7
;;;----------------------------------------------------

;========================================================
			_SJWD_RAM	DB ?
			_DW_RAM1		DB ?	; 0-12//40-100C
			AD_RAMH_OK	DB ?	;AD 
			
			_BIT1		DB ?	;1
				WARM		EQU		_BIT1.0	;
				ON_OFF		EQU		_BIT1.1	;
				T_ERR_DN	EQU		_BIT1.2	;
				T_ERR_KN	EQU		_BIT1.3	;				
				WIFF_NO		EQU		_BIT1.4	;
				WIFF_YES	EQU		_BIT1.5	;			
				WIFF_LK_	EQU		_BIT1.6	;				
				DJ_BIT		EQU		_BIT1.7
								
				_BIT2		DB ?	;2				
				SP		EQU		_BIT2.0	;
				K_BW	EQU		_BIT2.1
				K_B_BW	EQU		_BIT2.2
				K_BW_AB	EQU		_BIT2.3
				SP_BIT1	EQU		_BIT2.4
				SP_2	EQU		_BIT2.5
				SP_3	EQU		_BIT2.6
				SP_4	EQU		_BIT2.7
			
			;BW_TIME		EQU		[0E5H]	;	
			BW_TIME		DB ?	;
;==========================================================================
			RELAY	EQU		PB0		;
;==========================================================================


;================================================
CTM0_INT		.SECTION	AT 1CH	'CODE'
				;---------------------------

				JMP		DSZD1
 				RETI
;===================================================== 	

;=====================================================
AD_INT			.SECTION	AT 30H	'CODE'
				;---------------------------
				JMP		ADCL2
 				RETI		
;======================================================



                ;========================
                ;=USER PROGRAM          =
                ;========================
USER_PROGRAM    .SECTION   'CODE'

;;***********************************************************
;;*SUB. NAME:USER INITIAL PROGRAM                           *
;;*INPUT    :                                               *
;;*OUTPUT   :                                               *
;;*USED REG.:                                               *
;;*FUNCTION :                                               *
;;***********************************************************
                      ;;************************
_USER_PROGRAM_INITIAL_ASM:;;* USER_PROGRAM_INITIAL *
                      ;;************************
				;-----------IO-----------------
				MOV		A,00001010B
				MOV		PAC,A
				MOV		A,00000000B
 				MOV		PAPU,A
 				MOV		A,00000000B
 				MOV		PA,A
 				
 				MOV		A,11000110B
 				MOV		PBC,A 
 				MOV		A,11111110B
 				MOV		PB,A
 				CLR		PBPU		
			
		    	MOV		A,00000000B
 				MOV		PCC,A
		    	MOV		A,00000001B 				
 				MOV		PC,A
 				CLR		PCPU 		
				
				MOV		A,00000100B
 				MOV		PDC,A
 				MOV		A,00000100B
 				MOV		PD,A
 				CLR		PDPU 
 				
 				
 				;--------------------------
	
 				;---------------- 
				MOV		A,00100010B	;AD,AN2=0010 
				MOV		SADC0,A
				MOV		A,00001101B	;000= 01=VDD 101=FSYS/32
				MOV		SADC1,A					
				MOV		A,00000100B	;AN2
				MOV		ACERL,A		
				
				SET		ADCEN
				SET		ADE
				;-----------------
				
				;-----------------
				MOV     A,00010001B             ;FSYS  
				;MOV     A,00000000B             ;FSYS / 4 
                MOV     PTM0C0,A
                MOV     A,11000001B             ;Timer mode
                MOV     PTM0C1,A

                MOV     A,0E8H	;1110 1000	
                MOV     PTM0AL,A
                MOV     A,3H	;1/16*1000=62.5uS  11
                MOV     PTM0AH,A
  
                SET    PT0ON
                SET    PTM0AE 
				;-----------------
				SET		SP
 			
 				MOV		A,8D
 				MOV		_DW_RAM1,A
 				
				MOV		A,120D		;2H
 				MOV		BW_TIME,A 
 				
 				CLR		BW_TIME_1	
 				MOV		A,6DH		;
 				MOV		BW_TIME_2,A  
 				
 				SET		WIFF_NO
 				SET		EMI
                RET



;;***********************************************************
;;*SUB. NAME:USER_MAIN                                      *
;;*INPUT    :                                               *
;;*OUTPUT   :                                               *
;;*USED REG.:                                               *
;;*FUNCTION :                                               *
;;***********************************************************
                ;;********************
_USER_PROGRAM_ASM:  ;;USER PROGRAM ENTRY *
                ;;********************

                SNZ     _SCAN_CYCLEF
                RET
				;-------------------
	            CALL    _GET_KEY_BITMAP
	            
;=====================================================================================	            
	            
;============================	
AD_CLA:	
 	SZ		AD1
 	JMP		OUT_CL
	SET		AD1
	;---------------
	CLR		DISP_RAM1_A
	CLR		DISP_RAM2_A	
	CLR		DISP_RAM3_A	
	CLR		DISP_RAM4_A	
AD_K2:		
	MOV		A,DISP_RAM4_A
 	ADD		A,080H		
	MOV		TBLP,A 	
	TABRDL	T_DATA
		
	MOV		A,T_DATA
	SUB		A,AD_RAMH_OK	
	SZ		C
	JMP		AD_K1

	INC		DISP_RAM4_A		
		
	INC		DISP_RAM3_A
	MOV		A,DISP_RAM3_A
	SUB		A,10D
	SNZ		C
	JMP 	AD_K2
	CLR		DISP_RAM3_A	

	INC		DISP_RAM2_A
	MOV		A,DISP_RAM2_A
	SUB		A,10D
	SNZ		C
	JMP 	AD_K2
	CLR		DISP_RAM2_A
		
	MOV		A,1D
	MOV		DISP_RAM1_A,A
	;----------------
AD_K1:	
	CLR		EMI
	MOV		A,DISP_RAM4_A
	MOV		_SJWD_RAM,A	
	MOV		A,DISP_RAM3_A
	MOV		DISP_RAM3,A
	MOV		A,DISP_RAM2_A
	MOV		DISP_RAM2,A	
	MOV		A,DISP_RAM1_A
	MOV		DISP_RAM1,A
	SET		EMI
	JMP		OUT_CL	
;****************************************


;============OUT============================
OUT_CL:
	   SZ		ON_OFF
	   JMP		OUT_CL1_X
	   SZ		WARM	
	   JMP		OUT_CL1
	   ;----------------   
	   CLR		RELAY		;
	   CLR		YS_REAYT
	   CLR		K_ON1	   	
	   JMP		KEY_CL
;--------------------------------	   
OUT_CL1_X:
    SNZ		WARM
    JMP		OUT_CL1    
;------ONOFF+WARM-----------------------
	MOV		A,_SJWD_RAM	;
	SUB		A,100D		;100C
	SZ		C 	
 	JMP		OUT_CL4		;>=100
	
	SNZ		YS_REAYT 	;<100	
    SET		RELAY		; 
    SET		K_ON1
    JMP		KEY_CL     	
	;--------------
OUT_CL4:
	SET		SP_BIT1		
    SET		SP  
    CLR		SP_RAM          
	CLR		ON_OFF
	CLR		YS_REAYT
   	CLR		RELAY		;  
   	CLR		K_ON1
	SET		WT_100C		;100C
	SET		DW_TA		;100C
	JMP		KEY_CL	
;--------------------------------

;--------------------------------
OUT_CL1:		;
	MOV		A,060H		;
	ADD		A,_DW_RAM1	;
	MOV		TBLP,A 
	TABRDL	DW_WD_RAM 	
 
 	MOV		A,AD_RAMH_OK
	SUB		A,DW_WD_RAM
	SZ		C 	
 	JMP		OUT_CL3

	SNZ		YS_REAYT 	
    SET		RELAY			; 
	SET		K_ON1 
    JMP		KEY_CL     
	;------------------------------ 	    	 
OUT_CL3:
	SZ		WARM
	JMP		OUT_CL2

	SET		WT_100C	;	
		
    SET		SP_2 
    SET		SP  
    CLR		SP_RAM 
        
	CLR		ON_OFF
	CLR		YS_REAYT
   	CLR		RELAY		;  
   	CLR		K_ON1
 	JMP		KEY_CL
;------------------------------------------
OUT_CL2: 
	SZ		SP_BIT1
	JMP		OUT_CLA
		
	SZ		K_ON1 
	SET		WT_100C	;			
	SET		SP_BIT1
    SET		SP    		
	CLR		SP_RAM 
OUT_CLA:				 
   	CLR		RELAY		;  
  	SET		YS_REAYT
   	CLR		K_ON1  
 	JMP		KEY_CL		  		
;===========================================


;===================================================
;			 	            
;---------------------------------------------------
KEY_CL:
		; RESET
		; CHECK IF 2 BUTTONS PRESSED
		CLR		WIFF_LK_

        SNZ      _DATA_BUF[0].7	;ADD
        JMP     KEY_DEFAULT

        SNZ      _DATA_BUF[0].2	;SUB
		JMP		KEY_DEFAULT
		
		; SET
		SET		WIFF_LK_
		
		JMP     LP3
KEY_DEFAULT:


        SZ      _DATA_BUF[0].1	;START
        JMP		K_1      

		SZ      _DATA_BUF[0].6	;WARM
        JMP		K_2
      
        SZ      _DATA_BUF[0].7	;ADD
        JMP		K_3       
 
        SZ      _DATA_BUF[0].2	;SUB
        JMP		K_4        
        
LP3:	SZ		K_BIT1
		JMP		K_1C
	
		SZ		K_B_BW
		JMP		K_2C
					
	    CLR		KEY_1
	    CLR		KEY_1A
   		CLR		K_RAM1   
   		CLR		K_RAM2   		
   		CLR		K_RAM3   		
   		CLR		K_RAM4 
   		CLR		K_BW_AB
   		CLR		DW_TYPEQ
       	RET  
;--------------------------------	
  
;--------------------------------       	 
K_1:      ;START         
		SZ		T_ERR_DN
		RET
		SZ		T_ERR_KN
		RET
		   
        SZ		KEY_1
        RET
 		SZ		K_B_BW
 		RET 
 		
		SZ		K_BW
		JMP		k_1D
		
		SZ		WARM
		JMP		K_1B
		
		INC		K_RAM1
		MOV		A,K_RAM1
		SUB		A,5D	 
		SNZ		C
		RET 
		CLR		K_RAM1  
		            
        SET		KEY_1 
        SET		SP
        CLR		SP_RAM 

        SZ		ON_OFF
        JMP		K_1_A
        
        SET		ON_OFF 
		CLR		TIMEX
		CLR		WT_100C 
        CLR		K_BW
        
        SZ		DW_TYPE
        JMP		K_1E
        
 		MOV		A,12D
 		MOV		_DW_RAM1,A        		       
        RET	
        
K_1E:	CLR		DW_TYPE
		RET     
		;----------------   
K_1_A:  CLR		ON_OFF 
		CLR		WARM
        CLR		DW_TYPE	
        CLR		DW_TYPEQ
        CLR		K_BW
        CLR		SP_BIT1	
        SET		SP_2
        SET		SP_3
     	RET	  
     	;-------------  
K_1B:	SET		K_BIT1

		INC		K_RAM1
		MOV		A,K_RAM1
		SUB		A,125D	 
		SNZ		C
		RET 
		CLR		K_RAM1  
		
        SET		KEY_1 
        SET		SP
        CLR		SP_RAM 
        CLR		DW_TYPE
        CLR		K_BW
        
        SZ		ON_OFF
        JMP		K_1_A
        
        SET		ON_OFF 
		CLR		TIMEX
		CLR		WT_100C 
        CLR		DW_TYPE	
        CLR	    K_BIT1   
        RET	   
        ;--------------
K_1C:   SET		KEY_1 
        SET		SP
        CLR		SP_RAM 
        CLR		DW_TYPE 
        CLR		ON_OFF 
		CLR		WARM
        SET		SP_2
        SET		SP_3		
		CLR		SP_BIT1
		CLR		K_BIT1
  		CLR		K_RAM1   
   		CLR		K_RAM2   		
   		CLR		K_RAM3   		
   		CLR		K_RAM4 
        CLR		K_BW   				
     	RET    
     	;---------------
K_1D:	INC		K_RAM1
		MOV		A,K_RAM1
		SUB		A,5D	 
		SNZ		C
		RET 
		CLR		K_RAM1  
		
        CLR		DW_TYPE 
        CLR		K_BW
        SET		KEY_1    
        CLR		TIME1
		CLR		TIME2
		CLR		TIME3		 
		CLR		TIME3_A		
        SET		SP
        CLR		SP_RAM
        RET       		   	                   		     	 
;--------------------------------        
K_2:    ;WARM    
		SZ		T_ERR_DN
		RET
		SZ		T_ERR_KN
		RET
		
        SZ		KEY_1
        RET
        SZ		K_BIT1
        RET

		SZ		K_BW
		JMP		k_1D
        
  		SNZ		ON_OFF
  		JMP		K_2B
  		SNZ		WARM
  		RET 
  		 
K_2B:	SNZ		K_BW_AB
		SET		K_B_BW

		INC		K_RAM2
		
		MOV		A,K_RAM2
		SUB		A,50D	 
		SNZ		C
		RET
		CLR		K_B_BW  
		SET		K_BW_AB 		
		
		MOV		A,K_RAM2
		SUB		A,125D	 
		SNZ		C
		RET
		CLR		K_RAM2   

		SET		K_BW
		SET		DW_TYPE			
        SET		KEY_1 
        CLR		TIME1
		CLR		TIME2
		CLR		TIME3		 
		CLR		TIME3_A		
        SET		SP
        CLR		SP_RAM 	
        CLR		K_B_BW
		RET
		;------------------
K_2C:   SET		KEY_1 
		CLR		K_B_BW
        CLR		TIME1
		CLR		TIME2
		CLR		TIME3		 
		CLR		TIME3_A		
        SET		SP
        CLR		SP_RAM 

        SZ		WARM
        JMP		K_2_A
 		SET		WARM 
        SET		DW_TYPE	
        CLR		TIME1
        CLR		TIME2  
        CLR		TIME3       
        CLR		TIME3_A            
        
 		MOV		A,_DW_RAM1
		SUB		A,11D	 
		SNZ		C
		RET 
		MOV		A,11D
		MOV		_DW_RAM1,A       		               
        RET	
K_2_A:	CLR		WARM 
		CLR		ON_OFF
		CLR		DW_TYPE
        CLR		K_BW
        CLR		SP_BIT1
        SET		SP_2
        SET		SP_3        
        RET			        
;-------------------------------
K_3:	;ADD
		SZ		T_ERR_DN
		RET
		SZ		T_ERR_KN
		RET
		
        SZ		K_BIT1
        RET
        
 		SZ		K_B_BW
 		RET   
 		
        SZ		KEY_1
        RET
           
	    SNZ		KEY_1A
        JMP		LIAN_ADD
        
 		INC		K_RAM3
		MOV		A,K_RAM3
		SUB		A,20D	 
		SNZ		C
		RET 
		CLR		K_RAM3 
		 
		SET		DW_TYPEQ       
    	JMP		LA_CDA
         
LIAN_ADD:                  		        
		INC		K_RAM3
		MOV		A,K_RAM3
		SUB		A,5D	 
		SNZ		C
		RET 
		CLR		K_RAM3 
      	SET		SP
        CLR		SP_RAM   
        
       	SNZ		DW_TYPE	
       	JMP		ADD_A	 
        	
LA_CDA:
 		SET		KEY_1A
       	CLR		TIME3_A 
   
   		SZ		K_BW
   		JMP		BW_TIME_ADD   	
       	         		
		SZ		WARM
		JMP		K_3A
  		SZ		ON_OFF
  		RET		
  
K_3A:   CLR		SP_BIT1
		MOV		A,_DW_RAM1
		SUB		A,12D	 
		SZ		C
		JMP		$+2D 
		INC		_DW_RAM1  
		
		SNZ		WARM    	
    	RET   
 		MOV		A,_DW_RAM1
		SUB		A,11D	 
		SNZ		C
		RET 
		MOV		A,11D
		MOV		_DW_RAM1,A
		RET	   	 
   
ADD_A:  SET		DW_TYPE	
 		SET		KEY_1	
        RET	   
        
BW_TIME_ADD:
		SET		DW_TYPE	
        CLR		TIME1
        CLR		TIME2  
        CLR		TIME3       
        CLR		TIME3_A 	
				
        MOV		A,BW_TIME
        SUB		A,180D
        SNZ		C
        JMP		BW_TIME_ADD1
        MOV		A,180D
        MOV		BW_TIME,A
 		CLR		BW_TIME_1     	
 		MOV		A,4FH		
 		MOV		BW_TIME_2,A           
        RET          
BW_TIME_ADD1:
        MOV		A,BW_TIME
        SUB		A,120D
        SNZ		C
        JMP		BW_TIME_ADD2
        MOV		A,180D
        MOV		BW_TIME,A
 		CLR		BW_TIME_1     	
 		MOV		A,4FH		
 		MOV		BW_TIME_2,A           
        RET          
BW_TIME_ADD2:
        MOV		A,BW_TIME
        SUB		A,60D
        SNZ		C
        JMP		BW_TIME_ADD3
        MOV		A,120D
        MOV		BW_TIME,A
 		CLR		BW_TIME_1     	
 		MOV		A,6DH		
 		MOV		BW_TIME_2,A           
        RET                  
BW_TIME_ADD3:
        MOV		A,60D
        MOV		BW_TIME,A
 		CLR		BW_TIME_1     	
 		MOV		A,6H		
 		MOV		BW_TIME_2,A           
        RET                       	    		    	    
;----------------------------
K_4:	;SUB
		SZ		T_ERR_DN
		RET
		SZ		T_ERR_KN
		RET
		
        SZ		K_BIT1
        RET
 		SZ		K_B_BW
 		RET 
 		
        SZ		KEY_1
        RET
        	
        SNZ		KEY_1A
        JMP		LIAN_SUB
        	
 		INC		K_RAM4
		MOV		A,K_RAM4
		SUB		A,20D	 
		SNZ		C
		RET 
		CLR		K_RAM4  
		SET		DW_TYPEQ     
 		JMP		LIAN_DF
 		
LIAN_SUB: 		         		        
		INC		K_RAM4
		MOV		A,K_RAM4
		SUB		A,5D	 
		SNZ		C
		RET 
		CLR		K_RAM4  
		
        SET		SP
        CLR		SP_RAM			
       	SNZ		DW_TYPE 
       	JMP		SUB_A      		
LIAN_DF:
 		SET		KEY_1A		            
       	CLR		TIME3_A       	
       	
  		SZ		K_BW
   		JMP		BW_TIME_SUB

 		SZ		WARM
		JMP		K_4A
   		SZ		ON_OFF
  		RET     
  		   
K_4A:   SZ		_DW_RAM1
        JMP		$+2D	  
        RET	
        DEC		_DW_RAM1
        RET
SUB_A:      
        SET		DW_TYPE 
  		SET		KEY_1           
        RET  
        
BW_TIME_SUB:
		SET		DW_TYPE	
        CLR		TIME1
        CLR		TIME2  
        CLR		TIME3       
        CLR		TIME3_A 
		
        MOV		A,BW_TIME
        SUB		A,180D
        SNZ		C
        JMP		BW_TIME_SUB1
        MOV		A,120D
        MOV		BW_TIME,A
  		CLR		BW_TIME_1	
 		MOV		A,6DH		;
 		MOV		BW_TIME_2,A       
        RET 
BW_TIME_SUB1:
        MOV		A,BW_TIME
        SUB		A,120D
        SNZ		C
        JMP		BW_TIME_SUB2
        MOV		A,60D
        MOV		BW_TIME,A
  		CLR		BW_TIME_1	
 		MOV		A,6H		;
 		MOV		BW_TIME_2,A           
        RET     
BW_TIME_SUB2:
        MOV		A,30D
        MOV		BW_TIME,A
 		MOV		A,3FH		;0
 		MOV		BW_TIME_1,A     	
 		MOV		A,5BH		;5
 		MOV		BW_TIME_2,A           
        RET      
;-----------------------------
;-------------------------------------------------------------




;================1/16*1000=62.5uS=============================================	            
DSZD1:
  	;--------------------
 	MOV		ZD_RAM0,A 
 	MOV		A,STATUS
 	MOV		ZD_RAM1,A 	
 	;----------------------
  	CLR		PTM0AF
 	CLR     PTM0PF
 	;---------------------
 	INC		TIME1D
 	SNZ		TIME1D.1		;2*62.5=125uS
 	JMP		ZDCL_RET
 	CLR		TIME1D	
 	;---------------------
  	
;----------SP-----------------------
 		SNZ		SP
		JMP		DSQ1
		MOV		A,00000100B 	;PC2
        XORM	A,PC 
;---------------------------------------

;--------------------------------------
DSQ1:	INC		RAM1		;125*32=4MS
		MOV		A,RAM1
		SUB		A,32D	 
		SNZ		C
 		JMP		ZDCL_RET		
		CLR		RAM1    
		INC		_COUNTER4MS
;---------------------------------------
 		SNZ		SP
	 	JMP		DS_Z1
 	;-----------------
		INC		SP_RAM
		MOV		A,SP_RAM

		SNZ		SP_4
		JMP		SPP_C		
		SZ		SP_3
		JMP		SPP_A
SPP_C:	
		SUB		A,50D	;200MS 
		SNZ		C
		JMP		DS_Z1
SPP_B:
	  	CLR		SP	
 		CLR		PC2 
 	  	CLR		SP_RAM
		CLR		RAM2 
		SZ		SP_2  
  	 	JMP		DS_ZX		
  	 	CLR		SP_3
 	  	CLR		SP_4 	
 	  	JMP		DS_Z1
DS_ZX:
		 SET	SP_4
  	 	JMP		DS_Z1	 	  	
   	;-------------
SPP_A:
 		SUB		A,125D	;500MS  
 		SZ		C
 		JMP		SPP_B
		JMP		DS_Z1	 		 
;------------------------------------	 

;---------------------------------------		
DS_Z1:	SNZ		SP_2
		JMP		FGA1
		
 		INC		RAM2		;
		MOV		A,RAM2
		SUB		A,60D	 
		SNZ		C
		JMP		FGA1			
		CLR		RAM2	
		SET		SP
  		CLR		SP_RAM			
		CLR		SP_2		
;--------------------------------------

;--------------------------------------
FGA1:
	clr   	START 		;4MSAD
	set   	START       ; reset A/D 
	clr   	START       ; start A/D   	
;---------------------------------------- 	

;--------------TYPE------------------

;-------------------------
TYPE_B:
	CLR		PA7		;C	
	CLR		PA4		;G			
	CLR		PA2		;F	
	CLR		PA0		;E
	CLR		PD5		;D	
	CLR		PD4		;B	
	CLR		PC1		;A	
		
	SET		PC0		;COM1
	SET		PB5		;COM2
	SET		PB4		;COM3
	SET		PB3		;COM4
	SET		PD3		;COM5

	SZ		TYPE_BIT1
	JMP		TYA
	SZ		TYPE_BIT2
	JMP		TYB
	SZ		TYPE_BIT3
	JMP		TYC
;	SZ		TYPE_BIT4
;	JMP		TYD	
	;---------------------
;BIT1	1
	SET		TYPE_BIT1

	SNZ		T_ERR_DN
	JMP		$+4D
	MOV		A,01000000B
	MOV		DISP_RAM,A
 	JMP		T_B_A	

	SNZ		T_ERR_KN
	JMP		$+4D
	MOV		A,01111001B ;E
	MOV		DISP_RAM,A
 	JMP		T_B_A	

 	SNZ		K_BW
	JMP		$+4D
	MOV		A,BW_TIME_1
	MOV		DISP_RAM,A
 	JMP		T_B_A		

	SZ		DW_TYPE
	JMP		WD_TYPA1

	SNZ		DW_TA
	JMP		$+4D
	MOV		A,6H	;1
	MOV		DISP_RAM,A
 	JMP		T_B_A		

	SZ		WT_100C
	JMP		SET_T1	

CBY_1:	
	SZ		DISP_RAM1	
	JMP		T_B_B
	CLR		DISP_RAM
	JMP		T_B_A
T_B_B:		
	MOV		A,0F0H		;
	ADD		A,DISP_RAM1
	MOV		TBLP,A 
	TABRDL	DISP_RAM
T_B_A:		
	CALL	TYPE_DS	
	CLR		PC0   ;COM1
 	JMP		JSQ1
;---------------------- 	
WD_TYPA1:
	SZ		DW_TYPEQ
	JMP		$+3D
	SZ		TIME1.7
 	JMP		JSQ1
 		
SET_T1: 	
	MOV		A,070H		;
	ADD		A,_DW_RAM1	;
	MOV		TBLP,A 
	TABRDL	DISP_RAM
	
	SZ		DISP_RAM
	JMP		$+4D
	MOV		A,6H		;1 =100C
	MOV		DISP_RAM,A
	JMP		T_B_A
	
	CLR		DISP_RAM
	JMP		T_B_A	
;----------------------	----------------	 	
 	
;---------------------------------------
TYA:
	CLR		TYPE_BIT1	 	
	SET		TYPE_BIT2

	SNZ		T_ERR_DN
	JMP		$+4D
	MOV		A,01000000B
	MOV		DISP_RAM,A
 	JMP		T_B_C	

	SNZ		T_ERR_KN
	JMP		$+4D
	MOV		A,3FH	;0
	MOV		DISP_RAM,A
 	JMP		T_B_C	
 
	SNZ		K_BW
	JMP		$+4D
	MOV		A,BW_TIME_2
	MOV		DISP_RAM,A
 	JMP		T_B_C		
  	
	SZ		DW_TYPE
	JMP		WD_TYPA2

	SNZ		DW_TA
	JMP		$+4D
	MOV		A,3FH	;0
	MOV		DISP_RAM,A
	JMP		T_B_C	

	SZ		WT_100C
	JMP		SET_T2	

CBY_2:		
	SZ		DISP_RAM2
	JMP		T_B_D
	SZ		DISP_RAM1
	JMP		T_B_D
	CLR		DISP_RAM
	JMP		T_B_C	
T_B_D:			
	MOV		A,0F0H		;
	ADD		A,DISP_RAM2
	MOV		TBLP,A 	
	TABRDL	DISP_RAM
T_B_C:	
	CALL	TYPE_DS	
	CLR		PB5   ;COM2
  	JMP		JSQ1
  	;---------
WD_TYPA2:
	SZ		DW_TYPEQ
	JMP		$+3D
	SZ		TIME1.7
 	JMP		JSQ1
SET_T2: 	
	MOV		A,070H		;
	ADD		A,_DW_RAM1	;
	MOV		TBLP,A 
	TABRDL	DISP_RAM
	
	SZ		DISP_RAM
	JMP		$+4D
	MOV		A,3FH
	MOV		DISP_RAM,A
	JMP		T_B_C  
		
  	MOV		A,11110000B
  	ANDM	A,DISP_RAM
  	SWAP	DISP_RAM
  	MOV		A,0F0H		;
	ADD		A,DISP_RAM
	MOV		TBLP,A 	
	TABRDL	DISP_RAM
 	JMP		T_B_C 	
;----------------------	


;----------------------
TYB:
	CLR		TYPE_BIT2	 	
	SET		TYPE_BIT3

	SNZ		T_ERR_DN
	JMP		$+4D
	MOV		A,01000000B
	MOV		DISP_RAM,A
 	JMP		T_B_E	

	SNZ		T_ERR_KN
	JMP		$+4D
	MOV		A,6H	;1
	MOV		DISP_RAM,A
 	JMP		T_B_E	

	SNZ		K_BW		;
	JMP		$+4D
	MOV		A,72H		;h
	MOV		DISP_RAM,A
 	JMP		T_B_E		
		
	SZ		DW_TYPE
	JMP		WD_TYPA3	;

	SNZ		DW_TA
	JMP		$+4D
	MOV		A,3FH	;0
	MOV		DISP_RAM,A
	JMP		T_B_E		

	SZ		WT_100C
	JMP		SET_T3	

CBY_3:		
	MOV		A,0F0H		;
	ADD		A,DISP_RAM3	;
	MOV		TBLP,A 	
	TABRDL	DISP_RAM
T_B_E:	
	CALL	TYPE_DS	
	CLR		PB4   ;COM3
  	JMP		JSQ1
  	;-------------
WD_TYPA3:
	SZ		DW_TYPEQ
	JMP		$+3D
	SZ		TIME1.7		;0.5S
 	JMP		JSQ1
 	
SET_T3: 	
	MOV		A,070H		;
	ADD		A,_DW_RAM1	;
	MOV		TBLP,A 
	TABRDL	DISP_RAM
	
	SZ		DISP_RAM
	JMP		$+4D
	MOV		A,3FH		;0  =100C
	MOV		DISP_RAM,A
	JMP		T_B_E 
	 	
  	MOV		A,00001111B
  	ANDM	A,DISP_RAM
  	MOV		A,0F0H		;
	ADD		A,DISP_RAM
	MOV		TBLP,A 	
	TABRDL	DISP_RAM
 	JMP		T_B_E 	
;----------------------		  	
  	
;----------------------	 
TYC:
	CLR		TYPE_BIT3	
;	SET		TYPE_BIT4
	
	SZ		ON_OFF
	SET		PD5		;D	
	SZ		WARM
	SET		PC1		;A 
	SZ		WIFF_NO
	SET		PD4
	SZ		WIFF_YES
	SET		PA7		;		
	CLR		PB3   ;COM4
  	JMP		JSQ1
 	;----------------------
;TYD:
;	CLR		TYPE_BIT4
		
;	SZ		ON_OFF
;	SET		PA4		;
;	SZ		WARM
;	SET		PD4		;
;	CLR		PD3		;COM5
;  	JMP		JSQ1	
;====================================
 
;-=================================== 
JSQ1:	
 	INC		TIME1		;4*250=1S
	MOV		A,TIME1
	SUB		A,250D	 
	SNZ		C
 	JMP		ZDCL_RET 
	CLR		TIME1
	;--------------------
 	CLR		AD1		;1S
	;-------------------
;	SNZ		ON_OFF
 ;	JMP		ZDCL_RET 
 			
 ;	INC		TIME1XX		;
;	MOV		A,TIME1XX
;	SUB		A,240D	 
;	SNZ		C
 ;	JMP		ZDCL_RET 
;	CLR		TIME1XX

;	CLR		ON_OFF
;	CLR		WARM
;---------------------------------------	
	SNZ		DW_TYPE
	JMP		JSCL1
	
	INC		TIME3_A		;5S
	MOV		A,TIME3_A
	SUB		A,5D	 
	SNZ		C
 	JMP		JSCL1 
	CLR		TIME3_A	
	
	CLR		DW_TYPE
	CLR		K_BW
	JMP		JSCL1
;-------------------

;------------------
JSCL1:
	SNZ		YS_REAYT
	JMP		JSDF1
	INC		TIME2_D		;3S
	MOV		A,TIME2_D
	SUB		A,3D	 
	SNZ		C
 	JMP		JSDF1	 
	CLR		TIME2_D
	CLR		YS_REAYT	
;----------------------------	
JSDF1:
	SNZ		WT_100C
	JMP		CBY_A
	
	INC		TIMEX		;10S
	MOV		A,TIMEX
	SUB		A,10D	 
	SNZ		C
 	JMP		CBY_A 
	CLR		TIMEX
	
	CLR		WT_100C
	CLR		DW_TA
;-----------------------
CBY_A:
	SNZ		WARM
 	JMP		ZDCL_RET 

	INC		TIME2		;60S 1MIN
	MOV		A,TIME2
	SUB		A,60D		;	 
	SNZ		C
 	JMP		ZDCL_RET 
	CLR		TIME2	
		
	INC		TIME3		;
	MOV		A,TIME3
	SUB		A,BW_TIME	 
	SNZ		C
 	JMP		ZDCL_RET 
	CLR		TIME3	
	
	CLR		ON_OFF
	CLR		WARM
	CLR		SP_BIT1	
	CLR		K_BW
	CLR		DW_TYPE
    SET		SP_2 
    SET		SP  
    CLR		SP_RAM
 	JMP		ZDCL_RET   	 	  						
;============================= 	

;-----------------------------------
TYPE_DS:
	SZ		DISP_RAM.2
	SET		PA7		;C,H	
	SZ		DISP_RAM.6
	SET		PA4		;G	
	SZ		DISP_RAM.5
	SET		PA2		;F
	SZ		DISP_RAM.4
	SET		PA0		;E
	SZ		DISP_RAM.3
	SET		PD5		;D
	SZ		DISP_RAM.1
	SET		PD4		;B
	SZ		DISP_RAM.0
	SET		PC1		;A
	RET
;------------------------------------



;==============================================================	            
ADCL2:	            
	;--------------------
 	MOV		ZD_RAM0,A 
 	MOV		A,STATUS
 	MOV		ZD_RAM1,A 	
 	;----------------------
 	CLR		ADF
 	;---------------------
	SZ		AD_BIT1
	JMP		AD_X1
	SET		AD_BIT1
	mov  	a,SADOH 
	mov  	AD_RAMH,a
	MOV		A,31D
	MOV		AD_RAM_1,A		
	JMP		ZDCL_RET
;----------------------
AD_X1:
	mov  	a,SADOH 
	ADDM  	A,AD_RAMH
	SZ		C
	INC		AD_RAML
;---------------------	
	SDZ		AD_RAM_1		
	JMP		ZDCL_RET
	
	CLR		AD_BIT1	
	
	MOV		A,5D
	MOV		AD_RAM_1,A
	
	CLR		C
	RRC		AD_RAML 
	RRC		AD_RAMH 
	SDZ		AD_RAM_1
	JMP		$-4D	
;========================
	MOV		A,AD_RAMH
	MOV		AD_RAMH_OK,A
;---------------------------  
	MOV		A,AD_RAMH_OK	
	SUB		A,250D
	SZ		C
	JMP		ERR_DL
	
	MOV		A,5D
	SUB		A,AD_RAMH_OK
	SZ		C		
	JMP		ERR_WB
	CLR		T_ERR_KN
	CLR		T_ERR_DN
	JMP		AD_CLB		
	;---------------	
ERR_DL:
	SET		T_ERR_KN	;DN
	JMP		KDN_X	
ERR_WB:	
	SET		T_ERR_DN	;KN
KDN_X:	
	CLR		RELAY
	CLR		ON_OFF
	CLR		WARM
	JMP		ZDCL_RET		
;------------------------
 	
;------------------------
AD_CLB:		
	SNZ		ON_OFF
	JMP		ZDCL_RET	
	
	MOV		A,_SJWD_RAM	
	SUB		A,90D	;90C
	SNZ		C
	JMP		FBD2	;<90C

	MOV		A,AD_RAMH
	SUB		A,RT_DATA
	SNZ		C		
	JMP		FBA1
	MOV		A,AD_RAMH
	MOV		RT_DATA,A
	INC		RT_DATA
	CLR		RT_TIEM
	JMP		ZDCL_RET
		
FBA1:
	INC		RT_TIEM	
	MOV		A,RT_TIEM
	SUB		A,250D		;2MS*32*250=16S
	SNZ		C
 	JMP		ZDCL_RET		
	CLR		RT_TIEM  
	
	CLR		ON_OFF	; 
	CLR		RT_DATA	
    SET		SP_2
    CLR		SP_RAM     
   	CLR		RELAY		;  	
	SET		WT_100C   	
 	JMP		ZDCL_RET				
;------------------------------------
FBD2:
	CLR		RT_DATA
	CLR		RT_TIEM  
	JMP		ZDCL_RET
;-----------------------------------
;=======================================
                
;=================================
ZDCL_RET:		;�A�STATUS
	MOV		A,ZD_RAM1
	MOV		STATUS,A 
	MOV		A,ZD_RAM0					
	RETI
;=====================================




;============================================
WD_ROM_S		.SECTION	AT 1F60H	'CODE'
;============================================
	DC	31D,41D,50D,60D,69D,79D,89D,100D,111D,122D,133D,144D,154D
	;	40C	45C 50C 55C 60C 65C 70C 75C  80C  85C  90C  95C  100C

;============================================
WD_TADA			.SECTION	AT 1F70H	'CODE'
;============================================
	DC	64D,69D,80D,85D,96D,101D,112D,117D,128D,133D,144D,149D,0D,0D
	;	40C	45C 50C 55C 60C 65C  70C  75C  80C  85C  90C  95C  100C 105C
;============================================
RT_TADA			.SECTION	AT 1F80H	'CODE'
;============================================	 
	; 
	DC	6D,6D,7D,7D,8D,8D,9D,9D,10D,10D,11D,11D,12D,12D,13D							;0-15C
	DC	14D,15D,16D,17D,18D,19D,20D,21D,22D,23D,24D,25D,26D,27D,28D					;16-30C  		
	DC	29D,30D,32D,33D,34D,35D,36D,38D,39D,40D,42D,43D,45D,46D,48D					;31-45C		
	DC	49D,51D,52D,54D,56D,57D,59D,61D,62D,64D,66D,68D,70D,72D,74D					;46-60C
	DC	75D,77D,79D,81D,83D,85D,87D,89D,91D,93D,95D,98D,100D,102D,104D				;61-75C		
	DC	106D,108D,110D,112D,114D,116D,118D,120D,123D,125D,127D,129D,131D,133D,135D	;76-90C
	DC	137D,139D,141D,143D,144D,146D,148D,150D,152D,154D,156D,158D,159D,161D,163D  ;91-105C	
;--------------------------------------------------------------------------------------------

;============================================
SM_TADA			.SECTION	AT 1FF0H	'CODE'
;============================================	             
;  
	DC	3FH,6H,6DH,4FH,56H,5BH,7BH,0EH,7FH,5FH,00H,72H
;		0	1	2	3	4	5	6	7	8  9    h
;==================================================
;	DC	3FH,6H,5BH,4FH,66H,6DH,7DH,7H,7FH,6FH,00H