#pragma once

#include <stdint.h>

#ifndef PACKED
#define PACKED __attribute__((packed))
#endif

enum uart_frame_type {
  UART_FRAME_TYPE_STATUS = 0x81,
  UART_FRAME_TYPE_COMMAND = 0x71,
  UART_FRAME_TYPE_PAIRING = 0x82,
  UART_FRAME_TYPE_CONNECTION = 0x72,
};

enum uart_device_status {
  UART_DEVICE_STATUS_NOT_ON_BASE = 0xFF,
  UART_DEVICE_STATUS_IDLE = 0x00,
  UART_DEVICE_STATUS_PROGRESS = 0x01,
  UART_DEVICE_STATUS_KEEP = 0x02,
};

enum uart_device_mode {
  UART_DEVICE_MODE_NONE = 0x00,
  UART_DEVICE_MODE_STOP = UART_DEVICE_MODE_NONE,
  UART_DEVICE_MODE_BOIL_THEN_IDLE = 0x01,
  UART_DEVICE_MODE_BOIL_THEN_KEEP = 0x02,
  UART_DEVICE_MODE_WARM_THEN_IDLE = 0x03,
  UART_DEVICE_MODE_WARM_THEN_KEEP = 0x04,
  UART_DEVICE_MODE_PURIFY = 0xFF,
};

enum uart_wifi_pairing_action {
  UART_WIFI_PAIRING_ACTION_PAIRING = 0x50,
  UART_WIFI_PAIRING_ACTION_DIAGNOSTICS = 0xDD,
  UART_WIFI_PAIRING_ACTION_RESET = 0xEF,
};

enum uart_wifi_connection_state {
  UART_WIFI_CONNECTION_STATE_IDLE = 0x00,
  UART_WIFI_CONNECTION_STATE_FAILED = 0xFF,
  UART_WIFI_CONNECTION_STATE_CONNECTED = 0x01,
  UART_WIFI_CONNECTION_STATE_PAIRING = 0x50,
  UART_WIFI_CONNECTION_STATE_DIAGNOSTICS = 0xDD,
};

struct uart_device_status_frame {
  enum uart_device_status status:8;
  enum uart_device_mode mode:8;
  uint8_t temperature;
  struct {
    uint8_t _int;
    uint8_t _frac;
  } PACKED sensor_temperature;
} PACKED;

struct uart_device_command_frame {
  enum uart_device_mode mode:8;
  uint8_t temperature;
} PACKED;

struct uart_wifi_pairing_frame {
  enum uart_wifi_pairing_action action:8;
} PACKED;

struct uart_wifi_connection_frame {
  enum uart_wifi_connection_state state:8;
  uint8_t error_code;
} PACKED;

union uart_frame {
  uint8_t raw[8];
  struct {
    enum uart_frame_type type:8;
    union {
      uint8_t data[5];
      struct uart_device_status_frame status;
      struct uart_device_command_frame command;
      struct uart_wifi_pairing_frame pairing;
      struct uart_wifi_connection_frame connection;
    } PACKED;
    uint8_t crc;
    enum { UART_FRAME_END = 0x5A } end:8;
  } PACKED;
} PACKED;
