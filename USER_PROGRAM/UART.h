#ifndef _UART_H_
#define _UART_H_

#include <stdint.h>
#include "BS86D12C.h"
#include "UART_PROTOCOL.h"

#define BTN_CNT_PAIRING		1
#define BTN_CNT_DIAG		35
#define BTN_CNT_RESET		71
#define WIFI_LED_MODES		5
#define WIFI_LED_STEPS		8
#define BEEP_MODES			5
#define BEEP_STEPS			16


struct DeviceFlags {
	uint8_t WARM:1;			//	keep temperature
	uint8_t	BOIL:1;			//	boil
	uint8_t ERR_PROBE:1;	//	error probe
	uint8_t ERR_CIRCUIT:1;	//	damaged circuit
	uint8_t WIFI_NO:1;		//	blue, with wifi_no=1 light blue
	uint8_t WIFI_YES:1;		//	white
	uint8_t WIFI_LK:1;
	uint8_t DJ_BIT:1;
} PACKED;

struct DeviceFlags2 {
	uint8_t SPEAKER:1;		//	speaker control
	uint8_t reserved:7;
} PACKED;

struct uart_status {
	uint8_t temp_target;
	uint8_t temp_measured;
	uint8_t action;
	uint8_t frame_sent:1;
	uint8_t frame_received:1;
	uint8_t send_status:1;
	uint8_t one_second_flag:1;
	uint8_t ind_next:1;
	uint8_t boil:1;
	uint8_t warm:1;
	uint8_t beep_next:1;
} PACKED;

extern volatile uint8_t SJWD_RAM; 	//	measured temperature, 0-100 celsius
extern volatile uint8_t DW_RAM1;	//	set temperature, 0..12 (40..100 with step 5)
extern volatile struct DeviceFlags BIT1;		//	device flags
extern volatile struct DeviceFlags2 BIT2;		//	device flags
extern volatile uint8_t BW_TIME;	//	insulation time
extern volatile uint8_t COUNTER4MS;	//	4 ms counter

#define TEMP_MEASURED 	SJWD_RAM
#define TEMP_TARGET		DW_RAM1
#define device_flags	BIT1
#define device_flags2	BIT2
#define keep_time		BW_TIME
#define counter_4ms		COUNTER4MS


void UART_init(void);
void UART_handler(void);

#endif



