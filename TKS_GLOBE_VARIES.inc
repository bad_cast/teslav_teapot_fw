
;;******************************************
;;****TKS GLOBE VARIES DEFINE FOR ASM CODE *
;;******************************************
;-DEFINE LIBRARY VERSION
#define _V501_
;-SELECT IC BODY & INCLUDE REFERENCE FILE
#define _BS86D12C_



;-INCLUDE SYSTEM REFERENCE FILE
        #include BS86D12C.INC

;------------------------
;-DEFINE SYSTEM CLOCK   -
;------------------------
#define SystemClock             2       ;0=8MHZ
                                        ;1=12MHZ
                                        ;2=16MHZ


;--------------------------------
;-TOUCH KEY LIBRARY VARIES DEFINE
;--------------------------------
;-numeric operate ; range 0 ~ 15; 按鍵去抖設定5-7  值越大，按鍵反應越慢
#define DebounceTimes           6

;-numeric operate ; range 0 ~ 15; function: Time period to calibrate 自動校準時間設定4-8 設定的校正時間一到，且無按鍵時，即校正一次環境
#define AutoCalibrationPeriod   7       ;auto calibration period select 0=80ms .....15=1280ms

;-bit operate                   ; function: Sensitive double up 高感度設定 0?? =1感度放大一倍
#define HighSensitive           0       ;0=Normal ; 1=High Sensitive

;-numeric operate ; range 0 ~ 15; function: key holding time ,if time out will reset key status to non-press
#define MaximumKeyHoldTime      3       ;0=disable ; 1=4 secend ...... 15=60secend 按鍵最長反應時間1-3

;-bit operate ; range 0/1       ; function: Fasting internal singal filter speed led key response faster
#define FastResponse            0       ;0=Normal ; 1=Fast Response 快速反應設定0 ??

;-bit operate ; range 0/1       ; function: enalbe hardware hopping function
#define AutoFrequencyHopping    1       ;0=disable ; 1=enable 自動跳頻設定1

;-bit operate ; range 0/1       ; function: only one or all key active at the same time
#define OneKeyActive            0       ;0=all key active ; 1=one key active 單鍵輸出設定0 ??

;-bit operate ; range 0/1       ; function: Low power consumption 省電功能設定0 ??
#define PowerSave               0     ;0=disable ; 1=power save mode

;-bit operate ; range 0/1       ; function: Noise interfere protect to prevent false trigger
#define NoiseProtect            0       ;0=disable ; 1=enable 干擾保護設定1

;-bit operate ; range 0/1       ; function: moving calibration signal whatever key press or not 動態校正設定1
#define MovingCalibration       1       ;0=calibrate when key non press  ; 1=enable calibration whatever key press or not



;-Key threshold define
#define Key1Threshold           16     ;suggestion range 10 ~ 64 觸發閥值8-255   
#define Key2Threshold           16	;onoff
#define Key3Threshold           16	;sub -
#define Key4Threshold           16
;--
#define Key5Threshold           16
#define Key6Threshold           16
#define Key7Threshold           16	;power
#define Key8Threshold           16	;add +
;--
#define Key9Threshold           16
#define Key10Threshold           16
#define Key11Threshold           16
#define Key12Threshold           16

;---------------------------------
;-DEFINE PIN AS I/O OR TOUCH INPUT
;---------------------------------
#define IO_TOUCH_ATTR           00000000000000000000000011000110B;0=IO ; 1=TOUCH INPUT
                           ;KEY 3   ~   2 ~ 2 ~ 1 ~ 1 ~ 0 ~ 0 ~0 ;KEY32~KEY1
                           ;    2       4   0   6   2   8   4  1


;-----------------------------------------
;-conbine above varies in GlobeOptionA/B/C
;-----------------------------------------
#define GlobeOptionA            (AutoCalibrationPeriod<<4)|DebounceTimes
#define GlobeOptionB            (MaximumKeyHoldTime<<4)|HighSensitive
#define GlobeOptionC            (MovingCalibration<<7)|(PowerSave<<6)|(OneKeyActive<<5)|(NoiseProtect<<4)|(AutoFrequencyHopping<<3)|(FastResponse<<2)



