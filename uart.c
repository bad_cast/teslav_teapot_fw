#include "uart-protocol.h"

#include <stdint.h>
//#include <stdbool.h>

static uint8_t UART_crc(const union uart_frame *frame) {
  uint8_t sum = 0;
  uint8_t byte = 0;
  for (byte = 0; byte < sizeof(union uart_frame); byte++) {
    if (byte != (&frame->crc - &frame->raw[0])) {
      sum += frame->raw[byte];
    }
  }
  return sum;
}

static void UART_tx_byte(uint8_t byte) {
  // TODO(platform): Send one byte to hardware UART FIFO
  (void)byte;
}

// Transmit prepared UART frame
static void UART_tx_frame(union uart_frame *frame) {
  frame->end = UART_FRAME_END;
  frame->crc = UART_crc(frame);
  uint8_t byte = 0;
  for (byte = 0; byte < sizeof(union uart_frame); byte++) {
    UART_tx_byte(frame->raw[byte]);
  }
}

// TODO(device): call UART_send_status from any points where it changed (e.g. button/sensor handlers)
// TODO(device): call UART_send_status from timer handler (each 1 second)
void UART_send_status() {
  union uart_frame frame;
  frame.type = UART_FRAME_TYPE_STATUS;
  if (0) { // TODO(device): check that kettle is on base station
    if (0) { // TODO(device): check that kettle is executing program right now
      if (0) { // TODO(device): check that kettle is finished program and keeping temperature
        frame.status.status = UART_DEVICE_STATUS_KEEP;
      } else {
        frame.status.status = UART_DEVICE_STATUS_PROGRESS;
      }
      // TODO(device): get device execution mode (enum uart_device_mode)
      frame.status.mode = UART_DEVICE_MODE_NONE;
      // TODO(device): get device set temperature (for warming / keep)
      frame.status.temperature = 0;
    } else {
      frame.status.status = UART_DEVICE_STATUS_IDLE;
      frame.status.mode = UART_DEVICE_MODE_NONE;
      frame.status.temperature = 0;
    }
    // TODO(device): get device sensor temperature
    float sensor_temperature = 0;
    frame.status.sensor_temperature._int = (uint8_t)sensor_temperature;
    frame.status.sensor_temperature._frac = ((uint8_t)(sensor_temperature * 100.f)) % 100;
  } else {
    frame.status.status = UART_DEVICE_STATUS_NOT_ON_BASE;
    frame.status.mode = UART_DEVICE_MODE_NONE;
    frame.status.temperature = 0;
    frame.status.sensor_temperature._int = 0;
    frame.status.sensor_temperature._frac = 0;
  }
  UART_tx_frame(&frame);
}

// TODO(device): call UART_send_action(UART_WIFI_PAIRING_ACTION_PAIRING) for pairing button(s) press
// TODO(device): call UART_send_action(UART_WIFI_PAIRING_ACTION_DIAGNOSTICS) for diagnostics button(s) press
// TODO(device): call UART_send_action(UART_WIFI_PAIRING_ACTION_RESET) for reset button(s) press
void UART_send_action(enum uart_wifi_pairing_action action) {
  union uart_frame frame;
  frame.type = UART_FRAME_TYPE_PAIRING;
  frame.pairing.action = action;
  UART_tx_frame(&frame);
}

static void UART_handle_command(const struct uart_device_command_frame *frame) {
  switch (frame->mode) {
    case UART_DEVICE_MODE_STOP:
      // TODO(device): stop program that executing now, return to IDLE
      break;
    case UART_DEVICE_MODE_BOIL_THEN_IDLE:
      // TODO(device): set device program to boil (100 C), then switch off
      break;
    case UART_DEVICE_MODE_BOIL_THEN_KEEP:
      // TODO(device): set device program to boil (100 C), then keep temperature at frame->temperature
      (void)frame->temperature;
      break;
    case UART_DEVICE_MODE_WARM_THEN_IDLE:
      // TODO(device): set device program to warm upto frame->temperature, then switch off
      (void)frame->temperature;
      break;
    case UART_DEVICE_MODE_WARM_THEN_KEEP:
      // TODO(device): set device program to warm upto frame->temperature, then keep it
      (void)frame->temperature;
      break;
    case UART_DEVICE_MODE_PURIFY:
      // TODO(device): set device program to purify water (boil water, then keep boiling for 5 minutes)
      break;
  }
  // Send current (updated) device status to UART
  UART_send_status();
}

static void UART_handle_connection(const struct uart_wifi_connection_frame *frame) {
  switch (frame->state) {
    case UART_WIFI_CONNECTION_STATE_IDLE:
      // TODO(device): set connection indication mode to "IDLE"
      break;
    case UART_WIFI_CONNECTION_STATE_FAILED:
      // TODO(device): set connection indication mode to "FAILURE"
      break;
    case UART_WIFI_CONNECTION_STATE_CONNECTED:
      // TODO(device): set connection indication mode to "CONNECTED"
      break;
    case UART_WIFI_CONNECTION_STATE_PAIRING:
      // TODO(device): set connection indication mode to "PAIRING"
      break;
    case UART_WIFI_CONNECTION_STATE_DIAGNOSTICS:
      // TODO(device): set connection indication mode to "DIAGNOSTICS"
      break;
  }
}

// UART input handler
static void UART_handle_frame(const union uart_frame *frame) {
  switch (frame->type) {
    case UART_FRAME_TYPE_COMMAND:
      return UART_handle_command(&frame->command);
    case UART_FRAME_TYPE_CONNECTION:
      return UART_handle_connection(&frame->connection);
    default:
      return;
  }
}

static unsigned char UART_rx(uint8_t *byte) {
  // TODO(platform): read single byte from hardware UART FIFO
  *byte = 0;
  return 0;
}

// UART interrupt handler
void UART_ISR() {
  static union uart_frame frame;
  static uint8_t frame_offset = 0;

  uint8_t byte;
  uint8_t b;
  // Read next byte from FIFO
  while (UART_rx(&byte)) {
    // Write byte to buffer
    frame.raw[frame_offset++] = byte;
    // Check that we read full frame
    if (frame_offset != sizeof(frame)) continue;
    // Check frame header, end, checksum
    if ((frame.type != UART_FRAME_TYPE_COMMAND && frame.type != UART_FRAME_TYPE_CONNECTION) ||
        frame.end != UART_FRAME_END ||
        frame.crc != UART_crc(&frame)) {
      // Frame is not valid, so we skip to next byte
      for (b = 0; b < sizeof(frame) - 1; b++) {
        frame.raw[b] = frame.raw[b + 1];
      }
      frame_offset--;
      continue;
    }
    // Frame is valid, so we can handle it
    UART_handle_frame(&frame);
    // Reset frame offset to skip unnecessary memory moves
    frame_offset = 0;
  }
}

// UART init routine
void UART_init() {
  // TODO(platform): setup hardware UART to 9600-8N1
  // TODO(platform): setup UART interrupt handler
  // (void)UART_ISR;
}
